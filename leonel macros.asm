;Elaborado Angel Eduardo Cupul Osorio   

pantalla macro 
    mov ah,06
    mov al,0
    mov bh,37h
    mov cx,0000
    mov dx,184fh
    int 10h
     
    pantalla endm

recorrer macro
     
    mov ah, 05h
    mov al, 1
    int 10h
    
    recorrer endm
regresar macro
       
    mov ah, 05h
    mov al, 0
    int 10h
    
    regresar endm


datos segment
    cadena1 db 'MENU DE OPERACIONES$'
    cadena2 db '[1] SUMA, RESTA,MULTIPLICACION Y DIVISION$'
    cadena3 db '[2] DETERMINAR EL NUMERO MAYOR$'
    cadena4 db 'OPCION ERRONEA... PROGRAMA FINALIZADO$'
    cadena5 db 'INGRESE OPCION DESEADA, 0 PARA SALIR$'
    cadena6 db 'EL RESULTADO ES $'
    
    
     label1 DB "Selecciona una operacion $"
    label2 DB "1.- Suma $"
    label3 DB "2.- Resta $"
    label4 DB "3.- Multiplicacion $"
    label5 DB "4.- Division $"
    label6 DB "5.- Salir $"
    label7 DB "Ingrese una opcion $"
    label8 DB "Ingrese numero     $"
    label9 DB "El resultado es $"
    label10 DB "error no divisible entre 0 $"
    label11 DB "`cociente  $"
    label12 DB "residuo $"
    resultado DB 0
    cociente  DB    0
    residuo   DB    0
    numero    DB    0
    signox     DB    0
    r2      DB    ?
    ac      DB    0
    
    
    
    
    msj1 db 0ah,0dh, �Ingrese Tres digito del 0 al 9 : �, �$�
    msj2 db 0ah,0dh, �Primer Digito: �, �$�
    msj3 db 0ah,0dh, �Segundo Digito: �, �$�
    msj4 db 0ah,0dh, �Tercer Digito: �, �$�
    Mayor db 0ah,0dh, �El Digito Mayor Es: �, �$�

    Digito1 db 100 dup(�$�)
    Digito2 db 100 dup(�$�)
    Digito3 db 100 dup(�$�)

    salto db 009, '$' ;salto de linea
    
    
datos ends
            
 pila segment stack
        
    DW 256 DUP (?)

pila ends           
code segment  
    inicio:
    assume ds: datos, cs:code
    mov ax, datos
    mov ds, ax
    
    
  ;funcion 06h, int 10h: recorre la pantalla
  ;hacia arriba
  ;ah= funcion 06h
  ;al=00h para la pantalla completa
  ;bh=atributo de color fondo/texto
  ;cx=fila,columna iniciales
  ;dx=fila, columna finales
  
  mov ah, 06h
  mov al, 0    
  mov bh, 9fh
  mov cx, 0000d
  mov dx, 184fh
  int 10h
  
  mov ah, 06h
  mov al, 0    
  mov bh, 8fh
  mov cx, 0312h
  mov dx, 033fh
  int 10h
  
  mov ah, 06h
  mov al, 0    
  mov bh, 0afh
  mov cx, 0612h
  mov dx, 103fh
  int 10h
  
  ;funcion 02h, int 10h: establece posicion del cursor
    ;ah= funcion 02h
    ;bh= numero de pantalla
    ;dh  fila     solo hay de 0-24 filas
    ;dl= columna  solo hay de 0-79 columnas
    
    mov ah, 02h
    mov bh, 0
    mov dh, 03d
    mov dl, 30d
    int 10h 
    
    mov ah, 09h 
    mov dx, offset cadena1
    int 21h 
    
    
    mov ah, 02h
    mov bh, 0
    mov dh, 08d
    mov dl, 20d
    int 10h 
    
    mov ah, 09h 
    mov dx, offset cadena2
    int 21h 
    
    mov ah, 02h
    mov bh, 0
    mov dh, 10d
    mov dl, 20d
    int 10h 
    
    mov ah, 09h 
    mov dx, offset cadena3
    int 21h 
    

    
    mov ah, 02h
    mov bh, 0
    mov dh, 15d
    mov dl, 20d
    int 10h 
    
    mov ah, 09h 
    mov dx, offset cadena5
    int 21h 
    
      ; se crea un funcion para leer un caracter
    
    mov ah, 0h
    int 16h 
    
     ; se crea un switch para valorar el caracter ingresado
    
    cmp al,"1"
    je sumas

    cmp al,"2" 
    je restas
    
    mov ah, 02h
    mov bh, 0
    mov dh, 20d
    mov dl, 25d
    int 10h 
    
    mov ah, 09h 
    mov dx, offset cadena4
    int 21h
    
    mov ah, 4ch
    int 21h     ; finaliza el programa 



    
    
    ;se crea una etiqueta
    
    sumas:
    
    ;recorrer
   
    
   ; pantalla 
    
    ;--------------------------------------------------------------
        assume cs:code,ds:data,ss:pila
    push ds
    xor ax,ax
    push ax
    mov ax,datos
    mov ds,ax
    xor dx,dx
    
    ;interlineado
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h
    ;interlineado
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h          
    ;imprime seleccion de menu 
    mov ah,09h
    mov dx,offset label1
    int 21h 
    
    mov resultado,0; reseteo de la variable de la suma
    
    ;interlineado
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h  
    
    
    mov ah,09h
    mov dx,offset label2
    int 21h
    
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h
    
    mov ah,09h
    mov dx,offset label3
    int 21h
    
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h
    
    mov ah,09h
    mov dx,offset label4
    int 21h
    
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h
        
    mov ah,09h
    mov dx,offset label5
    int 21h
    
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h
    
    mov ah,09h
    mov dx,offset label6
    int 21h
    
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h
    
    mov ah,09h
    mov dx,offset label7
    int 21h
        
    ;lee teclado
    mov ah,01h
    int 21h
     
    ;ajunstando el teclado
    xor ah,ah
    sub al,30h
    mov cx,2
    ;verificando opcion
    
    cmp al,1
    jz suma ;se dirige al metodo suma
    
    cmp al,2
    jz resta ;se dirige al metodo resta
                                       
    cmp al,3
    jz mult ;se dirige al metodo multiplik
    
    cmp al,4
    jz divi ;se dirige al metodo dividir
    
    cmp al,5
    jz fin
    cmp al,6
    mov ah, 02h
    mov bh, 0
    mov dh, 20d
    mov dl, 25d
    int 10h 
    
    mov ah, 09h 
    mov dx, offset cadena4
    int 21h
    
    mov ah, 4ch
    int 21h  
    
suma: 
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h            
    mov ah,09h
    mov dx,offset label8
    int 21h
    
    ;lee teclado
    mov ah,01h
    int 21h
    
    ;verificando si es negativo
    cmp al,2dh
    je signo
       
    ;ajusta teclado
    sub al,30h
    add resultado,al 
    jmp return1
    

signo:
    mov ah,01h
    int 21h
    sub al,30h
    neg al
    add resultado,al
    je return1
 
return1: loop suma
         

imp1:
    cmp resultado,00
    jl imp2
    ;interlineado
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h
    mov AH,09H
    mov DX,OFFSET label9
    int 21H
    jmp imprime
        
       
imp2: 
    neg resultado 
    ;interlineado
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h
        
    mov AH,09H
    mov DX,OFFSET label9
    int 21H
    mov ah,02h        
    mov dl,'-'        
    int 21h
    jmp imprime
       
imprime:

               
        MOV AH,0
        MOV AL,resultado
        MOV CL,10
        DIV CL
        
        ADD AL,30H
        ADD AH,30H; CONVIRTIENDO A DECIMAL
        MOV BL,AH
        
        MOV DL,AL
        MOV AH,02H;IMPRIME LA DECENA
        INT 21H
        
        MOV DL,BL
        MOV AH,02H
        INT 21H;IMPRIME LA UNIDAD
        mov cx,2
       jmp menu sumas
resta:
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h            
    mov ah,09h
    mov dx,offset label8
    int 21h
    
    ;lee teclado
    mov ah,01h
    int 21h
    
    ;verificando si es negativo
    cmp al,2dh
    je signor
      
    ;ajusta teclado
    sub al,30h
    cmp cx,2
    je etiqueta1
    sub resultado,al 
    jmp return2
etiqueta1: mov resultado,al
            jmp return2    
signor:
    mov ah,01h
    int 21h
    sub al,30h
    neg al
    cmp cx,2
    je etiqueta1
    sub resultado,al
    je return2

return2: loop resta
    jmp imp1    
             
mult:
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h            
    mov ah,09h
    mov dx,offset label8
    int 21h
    
    ;lee teclado
    mov ah,01h
    int 21h
    
    ;verificando si es negativo
    cmp al,2dh
    je signom
    sub al,30h
    cmp cx,2
    je etiqueta2
    mov ah,0
    mul resultado
    jmp return3
etiqueta2:
    mov resultado,al
    jmp return3
signom:
    mov ah,01h
    int 21h
    sub al,30h
    neg al
    cmp cx,2
    je etiqueta2
    mov ah,0
    mul resultado
    jmp return3
return3:loop mult
        mov resultado,al
        jmp imp1    

mov signox,0    
divi:
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h            
    mov ah,09h
    mov dx,offset label8
    int 21h
    
    ;lee teclado
    mov ah,01h
    int 21h
    
    ;verificando si es negativo
    cmp al,2dh
    je signod
    
    sub al,30h
    cmp cx,2
    je etiqueta3
    cmp al,0
    je falla
    mov ah,0
    mov numero,al
    mov al,resultado
    div numero 
    jmp return4

etiqueta3:
    mov resultado,al
    jmp return4
signod:
    mov ah,01
    int 21h
    sub al,30h
    inc signox
    cmp cx,2
    je etiqueta3
    mov ah,0
    mov numero,al
    mov al,resultado
    div numero
    jmp return4

return4:loop divi
    mov cociente,al
    mov residuo,ah
    mov resultado,al
    jmp imp3
falla: 
    mov ah,9
    mov dx, offset label10
    int 21h
    jmp divi
imp3:
    
    
    mov ah,02h
    mov dl,10
    int 21h
    mov ah,02h
    mov dl,13
    int 21h
    mov AH,09H
    mov DX,OFFSET label9
    int 21H
    jmp imprimedivi
        
       

    
imprimedivi:
       MOV AL,resultado
              
       MOV CH,30H
       ADD AL,CH
       ADD AH,CH
       MOV BL,AH  
       
      
       MOV AH,9
       MOV DX,OFFSET label11
       INT 21H      
       
       cmp signox,1
       jz cambio
       jmp termina
       
cambio:
       mov dl,"-"
       mov ah,02h
       int 21h        
       mov signox,0

termina:
       MOV DX,0
       ADD cociente,30H
       MOV DL,cociente
       MOV AH,02H ;IMPRIME EL COCIENTE
       INT 21H
               
               
       MOV AH,9
       MOV DX,OFFSET label12
       INT 21H
       
       MOV DX,0
       ADD residuo,30H
       MOV DL,residuo 
       MOV AH,02H ;IMPRIME EL RESIDUO
       INT 21H
       
       jmp sumas 
fin:     ret 
    ;--------------------------------------------------------------
     
    
    
    jmp suma
    
     ;-------------7//////////////////////////////
    restas:
    ;recorrer 
    ;pantalla
    
    mov ah, 02h
    mov bh, 0
    mov dh, 20d
    mov dl, 05d
    int 10h
    
mov ah,09
mov dx,offset msj1 ;Imprimimos el msj1
int 21h

call saltodelinea;llamamos el metodo saltodelinea.

call pedircaracter ;llamamos al metodo

mov Digito1,al ;lo guardado en AL a digito1

call saltodelinea

call pedircaracter

mov Digito2,al

call saltodelinea

call pedircaracter

mov Digito3,al

call saltodelinea

;*******************************COMPARAR*****************************************

mov ah,digito1
mov al,digito2
cmp ah,al ;compara primero con el segundo
ja compara-1-3 ;si es mayor el primero, ahora lo compara con el tercero
jmp compara-2-3 ;si el primero no es mayor,ahora compara el 2 y 3 digito
compara-1-3:
mov al,digito3 ;ah=primer digito, al=tercer digito
cmp ah,al ;compara primero con tercero
ja mayor1 ;si es mayor que el tercero, entonces el primero es mayor que los 3

compara-2-3:
mov ah,digito2
mov al,digito3
cmp ah,al ;compara 2 y 3, YA NO es necesario compararlo con el 1,porque ya sabemos que el 1 no es mayor que el 2
ja mayor2 ;Si es mayor el 2,nos vamos al metodo para imprimirlo
jmp mayor3 ;Si el 2 no es mayor, obvio el 3 es el mayor

 
mayor1:

call MensajeMayor ;llama al metodo que dice: El digito mayor es:

mov dx, offset Digito1 ;Imprir El Digito 1 es mayor
mov ah, 9
int 21h
jmp exit

mayor2:
call MensajeMayor

mov dx, offset Digito2 ;Salto de linea
mov ah, 9
int 21h
jmp exit

mayor3:
call MensajeMayor

mov dx, offset Digito3 ;Salto de linea
mov ah, 9
int 21h
jmp exit

;********************************METODOS*****************************************

MensajeMayor:
mov dx, offset Mayor ;El digito Mayor es:
mov ah, 9
int 21h

ret
pedircaracter:
mov ah,01h; pedimos primer digito
int 21h
ret

saltodelinea:
mov dx, offset salto ;Salto de linea
mov ah, 9
int 21h
ret

exit:
mov ax, 4c00h;utilizamos el servicio 4c de la interrupcion 21h
int 21h ;para termianr el programa
    
    
    mov ah, 0h
    int 16h
    
   ; cmp al, 032
    jmp salir
    
    
    
    jmp resta

    
    salir:
       mov ah, 4ch
       int 21h
    
         ; finaliza el programa 
    jmp salir
    
  
  mov ah, 4ch
  int 21h     ; finaliza el programa 

  
   
    
    code ends

end inicio